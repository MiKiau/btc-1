#include <iostream>
#include <sstream>
#include <fstream>
#include <limits>
#include <random>
#include <chrono>

#define NUMBER_OF_OPTIONS 4

using std::cout; 
using std::cin;
using std::string;

class randomIntGenerator {
public:
    randomIntGenerator() = default;
    randomIntGenerator(int a, int b) {
        this->a = a;
        this->b = b;

        std::random_device rd;
        std::mt19937::result_type seed = rd() ^ (
            (std::mt19937::result_type)
            std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now().time_since_epoch()
                ).count() +
            (std::mt19937::result_type)
            std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::high_resolution_clock::now().time_since_epoch()
                ).count() );
        std::mt19937 gen(seed);
        this->rng = gen;

        std::uniform_int_distribution<std::mt19937::result_type> correctDist(a, b);
        this->dist = correctDist;
    }
    int randomInt() {
        return this->dist(rng);
    }

private:
    int a, b;
    std::mt19937 rng;
    std::uniform_int_distribution<std::mt19937::result_type> dist;
};

int getUserChoise();
string getFileText(const string filename);
string convertIntToHex(const int number);
string myHashFun(const string text);
string encodeAFile(const string filename);
void standartTesting();
void efficiencyTesting();
string generateRandomString(const int size);
string alterateOneLetterInString();
void generateFileForAdvanceTesting(const int pairsWithOneSymbolDiff);
void collisionTesting();
string convertHexCharToBinary(char hexSymbol);
string convertHexStringToBinary(string hexString);
double calculateSymbolDifference(string word1, string word2);
double calculateBitDifference(string hexWord1, string hexWord2);
void avalanchePercentageTesting();
void operateAccordingToChoise(const int choise);

int main(int argc, char **argv) {
    string  instructionMessage = "./main.exe -i OR ./main.exe -m <file name>\n",
            interfaceIntroMessage = "Please choose what to do:\n1. Begin standart testing.\n2. Check the efficiency.\n3. Do collision test.\n4. Do hex and bit difference test.\n",
            manualMode = "-m",
            interfaceMode = "-i";

    if (argc == 2 && interfaceMode.compare(argv[1]) == 0) {
        cout << interfaceIntroMessage;
        operateAccordingToChoise(getUserChoise());
    }
    else if (argc == 3 && manualMode.compare(argv[1]) == 0) {
        cout << encodeAFile(argv[2]) << "\n";
    }
    else {
        cout << instructionMessage;
    }
    return 0;
}

int getUserChoise() {
    int choise;
    while (true) {
        cout << "Your choise: ";
        cin >> choise;
        if (cin.good()
            && choise >= 0 
            && choise <= NUMBER_OF_OPTIONS) {
            // If the input is correct, then break.
            break;
        }
        else {
            // Clear the input and ask again.
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << "Please input a number (between 0 and " << NUMBER_OF_OPTIONS << ").\n";
        }
    }
    return choise;
}

string getFileText(const string filename) {
    std::stringstream sstr;
    std::ifstream fd(filename);
    string fileExtension = ".txt";
    if (filename.length() <= fileExtension.length() ||
        0 != filename.compare(
                filename.length() - fileExtension.length(), 
                fileExtension.length(), 
                fileExtension)
        ) {
        cout << "File must be a text (.txt).\n";
    }
    else if (fd.is_open()) {
        // Read file and put it into stringstream
        fd >> sstr.rdbuf();
    }
    else {
        cout << "File \"" << filename << "\" does not exist.\n";
        return "";
    }
    return sstr.str();
}

string convertIntToHex(const int number) {
    std::stringstream sstr;
    sstr << std::hex << number;
    return sstr.str();
}

string myHashFun(const string text) {
    string symbolTable = "abcdef0123456789";
    string result = "9ef18054cd817d43230b3644ccb69355c44ba372c49b81dacb6fa28ebb391e74";
    int a = 1295549,
        b = 37087,
        size = result.length();
    for (auto symbol : text) {
        for (int i = 0; i < size; i++) {
            int number = result[i] * (a + b) + symbol;
            number -= (i == 0) ? result[size - 1] : result[i - 1];
            a = a * b + symbol;
            if (a < 0) {
                a = -a * 1295;
            }
            result[i] = symbolTable[number % symbolTable.length()];
        }
    }
    return result;
}

string encodeAFile(const string filename) { 
    return myHashFun(getFileText(filename));
}

void standartTesting() {
    string standartTestingFiles[] = {
        "testTexts/1SymbolTextA.txt",
        "testTexts/1SymbolTextB.txt",
        "testTexts/1000RandomSymbolsText.txt",
        "testTexts/1000RandomSymbolsText2.txt",
        "testTexts/1000SymbolsText.txt",
        "testTexts/1000SymbolsText1SymbolDiff.txt",
        "testTexts/emptyFile.txt"
    };
    int hashSizeIs64 = 1,
        hashIsDeterministic = 1;

    for (auto fileName : standartTestingFiles) {
        string oldResult = encodeAFile(fileName);

        for (int i = 0; i < 10; i++) {
            string newResult = encodeAFile(fileName);
            hashIsDeterministic = 0 == oldResult.compare(newResult);
        }

        // cout << "Hash from file " << fileName << " is: " << oldResult << '\n';
        hashSizeIs64 = hashIsDeterministic && (64 == oldResult.length());
    }

    if (hashSizeIs64 && hashIsDeterministic) {
        cout << "The hash function can use any size input, it is determenistic and of 64 length.\n";
    }
    else {
        cout << "Hash function failed standart tests.\n";
    }
}

void efficiencyTesting() {
    std::ifstream fd("testTexts/konstitucija.txt");
    int lineCount = 0;
    double totalTimeSpent = 0.0;

    string line = "";
    while (getline(fd, line)) {
        auto start = std::chrono::high_resolution_clock::now();
        myHashFun(line);
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        
        lineCount++;
        totalTimeSpent += duration.count();
    }

    cout << "Hash function efficiency test results:\n";
    cout << "Total time spent: " << totalTimeSpent << "ms.\n";
    cout << "Hashing a line AVG time spent: " << totalTimeSpent / lineCount << "ms.\n";
}

string generateRandomString(const int size) {
    string  alphabet = "abcdefghijklmnopqrstuvwxz",
            result = "";
    randomIntGenerator gen(0, alphabet.length() - 1);
    for (int i = 0; i < size; i++) {
        result += alphabet[gen.randomInt()];
    }
    return result;
}

string alterateOneLetterInString(string initial) {
    string  alphabet = "abcdefghijklmnopqrstuvwxz";
    randomIntGenerator  lGen(0, initial.length() - 1),
                        aGen(0, alphabet.length() - 1);
    int location = lGen.randomInt();
    char oldSymbol = initial[location],
         newSymbol = alphabet[aGen.randomInt()];
    while (newSymbol == oldSymbol) {
        newSymbol = alphabet[aGen.randomInt()];
    }
    initial[location] = newSymbol;
    return initial;
}

void generateFileForAdvanceTesting(const int pairsWithOneSymbolDiff) {
    std::fstream file;
    file.open("testTexts/advanceTestingText.txt", std::ios::out);
    if (!file) {
        cout << "Failed to created an advance testing file.\n";
    }
    else {
        int wordSizes[] = {10, 100, 500, 1000},
            whichPart = 0;
        for (int i = 1; i <= 100000; i++) {
            string word1 = generateRandomString(wordSizes[whichPart]);
            string word2 = "";

            if (pairsWithOneSymbolDiff) {
                word2 = alterateOneLetterInString(word1);
            }
            else {
                word2 = generateRandomString(wordSizes[whichPart]);
            }

            file << word1 << " " << word2 << '\n';

            if (i % 25000 == 0) {
                whichPart++;
            }
        }
        file.close();
        cout << "Successfully generated advanceTestingText.txt.\n";
    }
}

void collisionTesting() {
    generateFileForAdvanceTesting(0);
    std::fstream fd("testTexts/advanceTestingText.txt");
    if (fd.is_open()) {
        int collisionCount = 0;
        string word1, word2;
        while (!fd.eof()) {
            fd >> word1 >> word2;
            if (0 == myHashFun(word1).compare(myHashFun(word2))) {
                collisionCount++;
            }
        }
        cout << "Hash function collision test results:\n";
        cout << "Collision percentage: " << (100 * collisionCount) / 100000 << "%\n";
    }
    else {
        cout << "Failed to do collision testing. There is no advancedTestingText.txt file.\n";
    }
}

string convertHexCharToBinary(char hexSymbol) {
    string result = "";
    switch(hexSymbol)
    {
        case '0':
            result = "0000";
            break;
        case '1':
            result = "0001";
            break;
        case '2':
            result = "0010";
            break;
        case '3':
            result = "0011";
            break;
        case '4':
            result = "0100";
            break;
        case '5':
            result = "0101";
            break;
        case '6':
            result = "0110";
            break;
        case '7':
            result = "0111";
            break;
        case '8':
            result = "1000";
            break;
        case '9':
            result = "1001";
            break;
        case 'a':
            result = "1010";
            break;
        case 'b':
            result = "1011";
            break;
        case 'c':
            result = "1100";
            break;
        case 'd':
            result = "1101";
            break;
        case 'e':
            result = "1110";
            break;
        case 'f':
            result = "1111";
            break;
        default:
            cout << "Error in convertHexCharToBinary conversion. Unknown char.\n";
    }
    return result;
}

string convertHexStringToBinary(string hexString) {
    string result = "";
    for (auto c : hexString) {
        result += convertHexCharToBinary(c);
    }
    return result;
}

double calculateSymbolDifference(string word1, string word2) {
    int differentSymbolCount = 0,
        size = word1.length();
    for (int i = 0; i < size; i++) {
        if (word1[i] != word2[i]) {
            differentSymbolCount++;
        }
    }
    return (100 * differentSymbolCount) / size;
}

double calculateBitDifference(string hexWord1, string hexWord2) {
    string  binaryWord1 = convertHexStringToBinary(hexWord1),
            binaryWord2 = convertHexStringToBinary(hexWord2);
    return calculateSymbolDifference(binaryWord1, binaryWord2);
}

void avalanchePercentageTesting() {
    generateFileForAdvanceTesting(1);
    std::fstream fd("testTexts/advanceTestingText.txt");
    if (fd.is_open()) {
        double  bitDiffMin = 100,
                bitDiffMax = 0.0,
                bitDiffTotal = 0.0;
        double  hexDiffMin = 100,
                hexDiffMax = 0.0,
                hexDiffTotal = 0.0;
        string word1, word2;
        while (!fd.eof()) {
            fd >> word1 >> word2;
            string hash1 = myHashFun(word1),
                   hash2 = myHashFun(word2);

            double bitDiff = calculateBitDifference(hash1, hash2);
            if (bitDiff < bitDiffMin)
                bitDiffMin = bitDiff;
            if (bitDiff > bitDiffMax)
                bitDiffMax = bitDiff;
            bitDiffTotal += bitDiff;

            double hexDiff = calculateSymbolDifference(hash1, hash2);
            if (hexDiff < hexDiffMin)
                hexDiffMin = hexDiff;
            if (hexDiff > hexDiffMax)
                hexDiffMax = hexDiff;
            hexDiffTotal += hexDiff;
        }
        cout << "Hash function avalanche percentage test results:\n";
        cout << "Bit difference percentage (min, avg, max): " << bitDiffMin << ' ' << bitDiffTotal / 100000 << ' ' << bitDiffMax << '\n';
        cout << "Hex difference percentage (min, avg, max): " << hexDiffMin << ' ' << hexDiffTotal / 100000 << ' ' << hexDiffMax << '\n';
    }
    else {
        cout << "Failed to do collision testing. There is no advancedTestingText.txt file.\n";
    }
}

void operateAccordingToChoise(const int choise) {
    switch (choise)
    {
    case 1:
        // Execute standart testing
        standartTesting();
        break;
    case 2:
        efficiencyTesting();
        break;
    case 3:
        collisionTesting();
        break;
    case 4:
        avalanchePercentageTesting();
        break;
    default:
        cout << "Error in operateAccordingToChoise function.\n";
        break;
    }
}