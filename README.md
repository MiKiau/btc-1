# Blockchain technology course 1-st

The first exercise in Blockchain Technology course. Its main goal is to try to create a hash function and test its capabilities.

## Instalation

- Use git clone command and copy the content of this repository;
- Then use local C/C++ compiler to compile main.exe from main.cpp;
- Run main.exe executable.

## Usage

This program supports two ways of usage:
1. Built-in testing interface;
2. Manual testing interface.

### Built-in testing interface

To use this mode, run the executable using this command:

```
./main -i
```

Then this interface this open:

![interface img](img/built-in interface.png)

To choose any of these options, simply press any the corresponding number.
Options explanations:
1. _Begin standart testing._

This mode will test hash function's:
- ability to take in any input;
- ability to always output determenistic output; 
- ability to always output 64 character output.

This mode uses files from testTexts folder. All of them except konstitucija.txt.

Possible result:

![Standart testing](img/1-standart-testing.png)


2. _Check the efficiency._

This mode will test hash function's time efficiency using konstitucija.txt
file from testTexts folder.

Possible result:

![Efficiency testing](img/2-efficiency-testing.png)


3. _Do collision test._

This mode will first create a file advancedTestingText.txt, which contains random two string pairs:
- 25000 strings of 10 characters length;
- 25000 strings of 100 characters length;
- 25000 strings of 500 characters length;
- 25000 strings of 1000 characters length;

Then it will hash these pairs individually and try to find hash pairs which are the same. In other words, it will output collision percentage.

Possible result:

![Collision testing](img/3-collision-testing.png)


4. _Do hex and bit difference test._

This mode will also create advancedTestingText.txt, but the pairs will differ by only 1 symbol.

Next it will hash these pairs individually and will try to find both hex and bit percentage difference between hash pairs. The output will be minimum, average and maximum difference percentages of both hex and bit differences.

Possible result:

![Difference testing](img/4-difference-testing.png)


### Manual mode

To use this mode, run the executable using this command:

```
./main -m <text file name>
```

Here is an example, where this mode was used with emptyFile.txt from testTexts directory. Command used:

```
./main -m testTexts/emptyFile.txt
```

Result:

![Manual mode example](img/manual-mode.png)
